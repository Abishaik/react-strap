import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Axios from "axios";

import TopNav from "./components/TopNav/TopNav";
import Home from "./components/Home/Home";
import Footer from "./components/Footer/Footer";
import DealerLocator from "./components/DealerLocator/DealerLocator";
import TestFlightForm from "./components/TestFlightForm/TestFlightForm";
import VehicleDetail from "./components/VehicleDetail/VehicleDetail";
import BuildAndPrice from "./components/BuildAndPrice/BuildAndPrice";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { vehicleData: null };
  }

  // Should be used for HTTP requests since it'll execute after render()
  // Can cause state changes & hence re-trigger render()
  componentDidMount() {
    Axios.get("http://localhost:3001/vehicles")
      .then((res) => {
        this.setState({ vehicleData: res.data });
      })
      .catch((error) => console.log(error));
  }

  render() {
    if (this.state.vehicleData) {
      return (
        <Router>
          <div className="App">
            <TopNav vehicleData={this.state.vehicleData} />
            <div className="content-area">
              <Route
                exact
                path="/"
                render={(props) => (
                  <Home {...props} vehicleData={this.state.vehicleData} />
                )}
              />
              <Route path="/find-a-dealer" component={DealerLocator} />
              <Route path="/schedule-test-flight" component={TestFlightForm} />
              <Route
                path="/detail/:selectedVehicle"
                render={(props) => (
                  <VehicleDetail
                    {...props}
                    vehicleData={this.state.vehicleData}
                  />
                )}
              />
                <Route path='/build-and-price' render={(props) => <BuildAndPrice {...props} vehicleData = {this.state.vehicleData} /> } />
            </div>
            <Footer />
          </div>
        </Router>
      );
    } else {
      return <h4>Loading data...</h4>;
    }
  }
}

export default App;
