import React, { Component } from "react";
import "./Footer.css";

class Footer extends Component {
 
  render() {
    return <footer>
        <a href="/schedule-test-flight">Schedule a test flight! No pilot licence required</a>
    </footer>
  }
}

export default Footer;
