import React, { Component } from "react";
import {
  Alert,
  InputGroup,
  InputGroupAddon,
  Card,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import Axios from "axios";

class TestFlightForm extends Component {
  constructor(props) {
    super(props);
    this.state = { showSuccess: false, showDanger: false };
  }

  handleInputChange = (eventData) => {
    const target = eventData.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    this.setState({ [name]: value });
  };

  onSubmit = (eventData) => {
    eventData.preventDefault();
    Axios.post("http://localhost:3001/mailingList", {
      customerName: this.state.customerName,
      email: this.state.email,
      phone: this.state.phone,
      budget: this.state.budget,
    })
      .then((res) => {
        this.setState({ showSuccess: true, showDanger: false });
      })
      .catch((err) => {
        this.setState({ showSuccess: false, showDanger: true });
      });
  };

  render() {
    return (
      <div>
        <Card>
          <CardBody>
            <CardTitle>Schedule a test flight</CardTitle>
            <CardSubtitle>No pilot's licence required!</CardSubtitle>
            <CardText>
              Fill out the below form fields to schedule a flight
            </CardText>
            <Form>
              <FormGroup>
                <Input
                  type="text"
                  name="customerName"
                  id="customerName"
                  placeholder="What is your name?"
                  onChange={this.handleInputChange}
                />
              </FormGroup>
              <br />
              <FormGroup>
                <Input
                  type="text"
                  name="phone"
                  id="phone"
                  placeholder="What is your contact no.?"
                  onChange={this.handleInputChange}
                />
              </FormGroup>
              <br />
              <InputGroup>
                <InputGroupAddon addonType="prepend">@</InputGroupAddon>
                <Input
                  type="text"
                  name="email"
                  id="email"
                  placeholder="What is your mail?"
                  onChange={this.handleInputChange}
                ></Input>
              </InputGroup>
              <InputGroup>
                <InputGroupAddon addonType="prepend">$</InputGroupAddon>
                <Input
                  type="text"
                  name="budget"
                  id="budget"
                  placeholder="What is your budget?"
                  onChange={this.handleInputChange}
                ></Input>
              </InputGroup>
            </Form>
            <br />
            <Button onClick={this.onSubmit}>Submit</Button>
            <Alert isOpen={this.state.showSuccess} color="success">
              Your data was successfully submitted. Your test flight awaits
            </Alert>
            <Alert isOpen={this.state.showDanger} color="danger">
              Something went wrong. Please try again later.
            </Alert>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default TestFlightForm;
